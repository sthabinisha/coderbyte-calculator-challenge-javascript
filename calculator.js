function calculate(input) {

  var f = {
    add: '+',
    sub: '-',
    div: '/',
    mlt: '*',
    mod: '%',
    exp: '^'
  };

  // Create array for Order of Operation and precedence
  f.ooo = [
    [
      [f.mlt],
      [f.div],
      [f.mod],
      [f.exp]
    ],
    [
      [f.add],
      [f.sub]
    ]
  ];

  input = /* replaceAll */ input.replace(new RegExp("\\s", 'g'), "");
   input = /* replaceAll */ input.replace(new RegExp("[)][(]", 'g'), ")*(");
   input = /* replaceAll */ input.replace(new RegExp("([0-9])[(]", 'g'), "$1*(");
   input = /* replaceAll */ input.replace(new RegExp("[)]([0-9])", 'g'), ")*$1");
    input = evalBracket(input);

  // input = input.replace(/[^0-9%^*\/()\-+.]/g,''); 
  //input = input.replace(/[^0-9%^*\/()\-+.]/g,'');           // clean up unnecessary characters

  var output= mathCalculation(input);
 

  function calc_internal(a, op, b) {
    a = a * 1;
    b = b * 1;
    switch (op) {
      case f.add:
        return a + b;
        break;
      case f.sub:
        return a - b;
        break;
      case f.div:
        return a / b;
        break;
      case f.mlt:
        return a * b;
        break;
      case f.mod:
        return a % b;
        break;
      case f.exp:
        return Math.pow(a, b);
        break;
      default:
        null;
    }
  }

  function evalBracket(str) {
     var openBracketIndex = 0;
    var result = str;
     for (let i = 0; i < str.length; i++) {
      var c = str.charAt(i);
      if (c == '(') {
        openBracketIndex = i;
      } else if (c == ')') {
      
        var calc = str.substring(openBracketIndex, i + 1);
        var betweenBrackets = calc.substring(1, calc.length - 1);
        var res = mathCalculation(betweenBrackets);
        console.log(calc);
        console.log(res);
             
       var startIndex = result.indexOf(calc);
       result.replace(startIndex, startIndex + calc.length(), res); 
       
      } 
    } 
    
    return result; 
  }
  
  
  function mathCalculation(input){
  var output;
   for (var i = 0, n = f.ooo.length; i < n; i++) {

    // Regular Expression to look for operators between floating numbers or integers
    var re = new RegExp('(\\d+\\.?\\d*)([\\' + f.ooo[i].join('\\') + '])(\\d+\\.?\\d*)');
    re.lastIndex = 0; // be cautious and reset re start pos

    // Loop while there is still calculation for level of precedence
    while (re.test(input)) {
      //document.write('<div>' + input + '</div>');
      output = calc_internal(RegExp.$1, RegExp.$2, RegExp.$3);
      if (isNaN(output) || !isFinite(output)) return output; // exit early if not a number
      input = input.replace(re, output);
    }
  }

  return output;
  
  
  }
}
